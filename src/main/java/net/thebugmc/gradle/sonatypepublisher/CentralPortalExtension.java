package net.thebugmc.gradle.sonatypepublisher;

import org.gradle.api.Action;
import org.gradle.api.Task;
import org.gradle.api.provider.Property;
import org.gradle.api.publish.VersionMappingStrategy;
import org.gradle.api.publish.maven.MavenPom;
import org.gradle.api.tasks.Optional;

/**
 * Configuration for the {@code sonatype-central-portal-publisher} plugin.
 */
public interface CentralPortalExtension {
    /**
     * The user token to use when publishing. Consider using {@code gradle.properties} instead with
     * the {@code centralPortal.username} property.
     *
     * @see #getPassword()
     */
    @Optional
    Property<String> getUsername();

    /**
     * The password to use when publishing. Consider using {@code gradle.properties} instead with
     * the {@code centralPortal.password} property.
     *
     * @see #getUsername()
     */
    @Optional
    Property<String> getPassword();

    /**
     * Sets the {@link PublishingType} for the current publication. The
     * {@link PublishingType#AUTOMATIC} is the default.
     */
    @Optional
    Property<PublishingType> getPublishingType();

    /**
     * Grabs the name that will be used throughout the Central Portal plugin, e.g. bundle name,
     * {@code artifactId}, etc. When not specified, {@code rootProject.name} will be used.
     */
    @Optional
    Property<String> getName();

    /**
     * The {@code :jar} task that will be used for generating the base {@code .jar} file (bundle
     * file 2/4).
     */
    @Optional
    Property<Task> getJarTask();

    /**
     * The {@code :javadoc} task that will be used for generating the {@code ...-javadoc.jar} file
     * (bundle file 3/4). Primarily used for Closed-source publications (when you want your JavaDoc
     * to be empty).
     */
    @Optional
    Property<Task> getJavadocJarTask();

    /**
     * The {@code :sourcesJar} task that will be used for generating the {@code ...-sources.jar}
     * file (bundle file 4/4). Primarily used for Closed-source publications (when you want your
     * sources to be empty).
     */
    @Optional
    Property<Task> getSourcesJarTask();

    /**
     * Configure POM (bundle file 1/4) with additional information, like {@code url},
     * {@code licenses}, {@code developers}, {@code scm}. Without this, the publication will most
     * likely be rejected.
     */
    void pom(Action<? super MavenPom> configure);

    /**
     * Configure <a href="https://docs.gradle.org/current/userguide/publishing_maven.html#publishing_maven:resolved_dependencies">version mapping strategy</a>.
     */
    void versionMapping(Action<? super VersionMappingStrategy> configure);
}
