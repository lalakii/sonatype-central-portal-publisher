package net.thebugmc.gradle.sonatypepublisher;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static java.lang.Math.min;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Task that calculates checksums for every non-{@code .asc} file in an archive.
 *
 * @see #calculateFor
 */
public class Checksum extends DefaultTask {
    private AbstractArchiveTask archive;

    /**
     * Specifies what file to calculate checksums for.
     */
    public void calculateFor(AbstractArchiveTask archive) throws IllegalStateException {
        if (this.archive != null && this.archive != archive)
            throw new IllegalStateException("Already calculating checksum for " + this.archive);

        dependsOn(archive);
        this.archive = archive;
    }

    public String toString() {
        return "Checksum for " + archive;
    }

    private static final int UPPER_DIGIT = 0xF0;
    private static final int LOWER_DIGIT = 0x0F;
    private static final int HEX_RADIX = 16;

    private static String hex(byte[] bytes) {
        var result = new StringBuilder();
        for (var b : bytes)
            result
                .append(Character.forDigit((b & UPPER_DIGIT) >> 4, HEX_RADIX))
                .append(Character.forDigit(b & LOWER_DIGIT, HEX_RADIX));
        return result + "";
    }

    /**
     * File in which the checksum output will be present.
     */
    public File outputFile() {
        var archiveFile = archive.getArchiveFile().get().getAsFile();
        var ext = "." + archive.getArchiveExtension().get();
        var name = archiveFile
            .getName()
            .substring(0, archiveFile.getName().length() - ext.length());
        return new File(archiveFile.getParentFile(), name + "-checksum" + ext);
    }

    private final HashMap<String, String> hashWithExt = new HashMap<>() {{
        put("MD5", ".md5");
        put("SHA-1", ".sha1");
        put("SHA-256", ".sha256");
        put("SHA-512", ".sha512");
    }};

    /**
     * Copy over files and add their checksums to the {@link #outputFile() output file}.
     *
     * @throws IOException If any IO exception occurs.
     */
    @TaskAction
    public void writeChecksums() throws IOException {
        var archiveFile = archive.getArchiveFile().get().getAsFile();
        try (var out = new FileOutputStream(outputFile())) {
            try (var zipFile = new ZipOutputStream(out)) {
                try (var oldZipFile = new ZipFile(archiveFile)) {
                    var entries = oldZipFile.entries();
                    while (entries.hasMoreElements()) {
                        var readEntry = entries.nextElement();
                        var data = oldZipFile.getInputStream(readEntry).readAllBytes();
                        zipFile.putNextEntry(readEntry);
                        zipFile.write(data);
                        zipFile.closeEntry();
                        if (!readEntry.isDirectory()) {
                            hashWithExt.forEach((alg, ext) -> addChecksumToZipEntry(zipFile, alg, readEntry.getName() + ext, data));
                        }
                    }
                }
            }
        }
    }

    private void addChecksumToZipEntry(ZipOutputStream out, String algorithm, String name, byte[] data) {
        try {
            out.putNextEntry(new ZipEntry(name));
            out.write(hex(MessageDigest.getInstance(algorithm).digest(data)).getBytes(UTF_8));
            out.closeEntry();
        } catch (NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}