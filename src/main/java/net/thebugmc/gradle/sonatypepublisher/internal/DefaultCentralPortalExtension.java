package net.thebugmc.gradle.sonatypepublisher.internal;

import net.thebugmc.gradle.sonatypepublisher.CentralPortalExtension;
import org.gradle.api.Action;
import org.gradle.api.publish.VersionMappingStrategy;
import org.gradle.api.publish.maven.MavenPom;
import org.gradle.api.reflect.HasPublicType;
import org.gradle.api.reflect.TypeOf;

import static org.gradle.api.reflect.TypeOf.typeOf;

public abstract class DefaultCentralPortalExtension implements CentralPortalExtension, HasPublicType {
    private Action<? super MavenPom> pomConfigurator;
    private Action<? super VersionMappingStrategy> versionMappingConfigurator;

    public void configurePom(MavenPom pom) {
        if (pomConfigurator != null)
            pomConfigurator.execute(pom);
    }

    public void configureVersionMapping(VersionMappingStrategy vms) {
        if (versionMappingConfigurator != null)
            versionMappingConfigurator.execute(vms);
    }

    public void pom(Action<? super MavenPom> configure) {
        pomConfigurator = configure;
    }

    public void versionMapping(Action<? super VersionMappingStrategy> configure) {
        versionMappingConfigurator = configure;
    }

    public TypeOf<?> getPublicType() {
        return typeOf(CentralPortalExtension.class);
    }
}
