package net.thebugmc.gradle.sonatypepublisher.platform;

import org.gradle.api.Project;
import org.gradle.api.UnknownDomainObjectException;
import org.gradle.api.component.SoftwareComponent;
import org.gradle.api.file.FileTree;
import org.gradle.api.logging.Logger;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.javadoc.Javadoc;
import org.gradle.jvm.tasks.Jar;

import static org.gradle.api.tasks.SourceSet.MAIN_SOURCE_SET_NAME;

/**
 * {@code java} platform that assumes Java plugin classes are available.
 */
public final class JavaPlatform implements Platform {
    public FileTree mainSourceSetSources(Logger log, Project target) {
        log.info("Detecting the `main` Java sources");
        return target
            .getExtensions()
            .getByType(SourceSetContainer.class)
            .getByName(MAIN_SOURCE_SET_NAME)
            .getAllSource();
    }

    public String defaultPackaging() {
        return "jar";
    }

    public Jar jarTask(Logger log, TaskContainer tasks) {
        log.info("Detecting the `:jar` task");
        return (Jar) tasks.getByName("jar");
    }

    public Javadoc javadocTask(
        Logger log,
        Project target,
        TaskContainer tasks,
        FileTree mainSourceSetSources,
        Provider<String> nameProvider
    ) {
        log.info("Detecting the `:javadoc` task");
        return (Javadoc) tasks.getByName("javadoc");
    }

    public SoftwareComponent component(
        Logger log,
        Project target
    ) throws UnknownDomainObjectException {
        log.info("Detecting the `java` component");
        return target.getComponents().getByName("java");
    }
}
